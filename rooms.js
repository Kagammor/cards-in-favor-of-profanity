module.exports = function(es6, config, io, game, util, note) {
    io.on('connection', function(socket) {
        socket.on('createRoom', function(name) {
            game.createRoom(name)
            .then(function(roomID) {
                socket.emit('redirect', '/game/' + roomID);
            })
            .catch(function(error) {
                note('cifop', 2, error);
            });
        });

        socket.on('getRooms', function() {
            game.getAllRooms()
            .then(function(rooms) {
                socket.emit('Rooms', rooms);
            });
        });
    });
};
