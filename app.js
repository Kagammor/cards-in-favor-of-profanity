var config = require('./config.json');

var es6 = require('es6-shim');

var io = require('socket.io').listen(3666);

var Sequelize = require('sequelize');
var sequelize = new Sequelize(config.database.database, config.database.user, config.database.password, { dialect: 'mysql', logging: config.database.logging, define: { timestamps: false }});
var models = require('./models.js')(Sequelize, sequelize);

var util = require('util');
var uid = require('./modules/uid.js');

var note = require('./modules/note.js');

var game = require('./game.js')(es6, config, io, sequelize, models, util, note, uid);

require('./server.js')(es6, config, io, game, util, note);
require('./rooms.js')(es6, config, io, game, util, note);
require('./interface.js')(es6, config, io, game, util, note);
require('./chat.js')(es6, config, io, game, util, note);
