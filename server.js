var app = require('express')();
var exphbs = require('express-handlebars');

module.exports = function(es6, config, io, game, util, note) {
    app.engine('handlebars', exphbs({defaultLayout: 'main'}));
    app.set('view engine', 'handlebars');

    app.get('/', function(req, res) {
        res.render('home', {'title': 'Home'});
    });

    app.get('/rooms', function(req, res) {
        res.render('rooms', {'title': 'Rooms'});
    });

    app.get('/game/:room', function(req, res) {
        var roomID = req.params.room;

        Promise.all([
            game.getRoom(roomID),
            game.getAllPacks(roomID)
        ])
        .then(function(roomData) {
            res.render('game', {'title': roomData[0].name, 'settings': config.settings, 'packs': roomData[1]});
        })
        .catch(function(error) {
            res.status(404);
            res.render('error', {'title': error});
        });
    });

    var server = app.listen(3600, function() {
        note('server', '0!', 'Listening at port ' + server.address().port);
    });
};
