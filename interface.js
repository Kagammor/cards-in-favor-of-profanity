module.exports = function(es6, config, io, Game, util, note) {
    io.on('connection', function(socket) {
        socket.user = socket.user || {'id': socket.id};

        socket.on('joinRoom', function(roomId) {
            Promise.all([
                Game.join(socket.user, roomId),
                Game.getSettings(roomId)
            ])
            .then(function(roomData) {
                socket.user.room = roomId;

                io.to(socket.id).emit('Settings', roomData[1], roomData[0]);
            })
            .catch(function(error) {
                note('cifop', 2, error.stack || error);
            });
        });

        socket.on('setSettings', function(newSettings) {
            new Promise(function(resolve, reject) {
                return Game.isHost(socket.id, socket.user.room) ? resolve() : reject('User is not host');
            })
            .then(Game.setSettings.bind(null, newSettings, socket.user.room))
            .then(function(settings) {
                io.to(socket.user.room).emit('Settings', settings);
            })
            .catch(function(error) {
                note('cifop', 2, error.stack || error);
            });
        });

        socket.on('startGame', function() {
            Game.start(socket.id, socket.user.room)
            .then(function(cards) {
                io.to(socket.user.room).emit('blackCard', cards[0]);

                Object.keys(cards[1]).forEach(function(userId) {
                    if(Game.isCzar(userId, socket.user.room)) {
                        io.to(userId).emit('Hand', {'cards': cards[1][userId], 'playable': false, 'czar': true});
                    } else {
                        io.to(userId).emit('Hand', {'cards': cards[1][userId], 'playable': true, 'czar': false});
                    }
                });
            })
            .then(Game.getAllUsers.bind(null, socket.user.room))
            .then(function(allUsers) {
                io.to(socket.user.room).emit('Users', allUsers);
            })
            .catch(function(error) {
                note('cifop', 2, error.stack || error);
            });
        });

        socket.on('playCards', function(cardIds) {
            Game.playCards(cardIds, socket.id, socket.user.room)
            .then(function(cards) {
                io.to(socket.id).emit('Hand', {'cards': cards.hand, 'playable': false, 'czar': false});
                io.to(socket.user.room).emit('playedCards', cards.played);
            })
            .catch(function(error) {
                note('cifop', 2, error.stack || error);
            });
        });

        socket.on('czar', function(winnerId) {
            if(Game.isCzar(socket.id, socket.user.room)) {
                Game.electWinner(winnerId, socket.user.room)
                .then(Game.getAllUsers.bind(null, socket.user.room))
                .then(function(allUsers) {
                    var winner = allUsers.find(function(user) {
                        return user.id === winnerId;
                    });

                    io.to(socket.user.room).emit('winner', winner.id);

                    io.to(socket.user.room).emit('chat', {
                        'type': 'broadcast',
                        'msg': winner.name + ' wins!',
                    });

                    io.to(socket.user.room).emit('Users', allUsers);
                })
                .then(Game.newRound.bind(null, socket.user.room, config.game.reviewTimeout))
                .then(function(cards) {
                    io.to(socket.user.room).emit('blackCard', cards[0]);

                    Object.keys(cards[1]).forEach(function(userId) {
                        if(Game.isCzar(userId, socket.user.room)) {
                            io.to(userId).emit('Hand', {'cards': cards[1][userId], 'playable': false, 'czar': true});
                        } else {
                            io.to(userId).emit('Hand', {'cards': cards[1][userId], 'playable': true, 'czar': false});
                        }
                    });
                })
                .catch(function(error) {
                    note('cifop', 2, error.stack || error);
                });
            }
        });

        socket.on('disconnect', function() {
            if(socket.user.room) {
                Game.leave(socket.id, socket.user.room)
                .then(function(decision) {
                    if(decision.czar) {
                        io.to(decision.czar.id).emit('Hand', {'cards': decision.czar.cards, 'playable': false, 'czar': true});
                    }

                    if(decision.cards) {
                        io.to(socket.user.room).emit('blackCard', decision.cards[0]);

                        Object.keys(decision.cards[1]).forEach(function(userId) {
                            if(Game.isCzar(userId, socket.user.room)) {
                                io.to(userId).emit('Hand', {'cards': decision.cards[1][userId], 'playable': false, 'czar': true});
                            } else {
                                io.to(userId).emit('Hand', {'cards': decision.cards[1][userId], 'playable': true, 'czar': false});
                            }
                        });
                    }

                    if(decision.host) {
                        return Game.getSettings(socket.user.room)
                        .then(function(settings) {
                            io.to(decision.host).emit('Settings', settings, true);
                        });
                    }
                })
                .catch(function(error) {
                    note('cifop', 2, error.stack || error);
                });
            }
        });
    });
};
