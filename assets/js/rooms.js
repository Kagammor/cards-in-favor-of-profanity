$(document).ready(function() {
    var socket = io('http://socket.cifop.unknown.name/');

    $('#roomCreate').on('click', function() {
        socket.emit('createRoom', $('#roomName').val());
    });
    $('#roomName').on('input', function() {
        if($('#roomName').val().replace(/[A-Za-z0-9?!#.,\s\-&()']/g, '').length > 0) {
            $('#roomName').addClass('input-error');
            $('#roomCreate').prop('disabled', true);
        } else {
            $('#roomName').removeClass('input-error');
            $('#roomCreate').prop('disabled', false);
        }
    });

    socket.emit('getRooms', true);

    socket.on('Rooms', function(rooms) {
        $('#roomlist').empty();

        for(var room in rooms) {
            var roomtile = $('<li class="nolist roomtile">');

            var roomname = $('<span class="roomname">');
            var roomlink = $('<a href="/game/' + room + '">');
            var roomusers = $('<span class="roomusers">');
            var roomstatus = $('<span class="roomstatus">');

            roomlink.text(rooms[room].name);
            roomname.append(roomlink);
            roomusers.text(Object.keys(rooms[room].users).length);
            roomstatus.text(rooms[room].status);

            roomtile.append(roomname);
            roomtile.append(roomusers);
            roomtile.append(roomstatus);

            $('#roomlist').append(roomtile);
        }
    });

    socket.on('redirect', function(path) {
        window.location = path;
    });
});
