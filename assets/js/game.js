$(document).ready(function() {
    var socket = io('socket.cifop.unknown.name'),
        room = window.location.pathname.split('/')[2],
        lastUsername,
        localCards = {
            'selected': [],
            'elected': null
        };

    socket.emit('joinRoom', room);

    $('.gameControlPanel').find('input').on('change', function() {
        var settings = {
            'room': {
                'spectators': $('#settingsSpectators').prop('checked'),
                'timeout': $('#settingsTimeout').prop('checked')
            },
            'game': {
                'players': $('#settingsPlayers').val(),
                'score': $('#settingsScore').val()
            },
            'packs': []
        };

        $('.settingsPacks').each(function(index, pack) {
            if($(pack).prop('checked')) {
                settings.packs = settings.packs.concat(pack.id);
            }
        });

        socket.emit('setSettings', settings);
    });

    $('#start').on('click', function() {
        socket.emit('startGame', true);
    });

    $('#showControlPanel').click(function() {
        $('#showControlPanel').toggleClass('dark');
        $('.gameControlPanel').toggleClass('hidden');
    });

    $('#writer').on('submit', function(e) {
        socket.emit('chat', {'msg': $('#writer > input').val()});

        $('#writer > input').val('');
        e.preventDefault();
    });

    socket.on('Settings', function(newSettings, isHost) {
        $('#settingsSpectators').prop('checked', newSettings.room.spectators);
        $('#settingsTimeout').prop('checked', newSettings.room.timeout);

        $('#settingsPlayers').val(newSettings.game.players);
        $('#settingsScore').val(newSettings.game.score);

        $('.settingsPacks').prop('checked', false);

        for(var pack in newSettings.packs) {
            $('#' + newSettings.packs[pack]).prop('checked', true);
        }

        if(isHost) {
            $('.gameControlBar').find('input').prop('disabled', false);
            $('#start').removeClass('hidden');
        }
    });

    socket.on('blackCard', function(card) {
        $('#playedCards').empty();

        $('#black').html(card.text.replace('? _', '?').replace(/_/g, '<span class="card-blank"></span>'));
        $('#black').removeClass('hidden');

        localCards.black = card;
    });
    socket.on('Hand', function(hand) {
        $('#hand').empty();
        localCards.selected = [];

        if(hand.czar) {
            $('#hand').append('<span>You are the czar!</span>');
        } else {
            hand.cards.forEach(function(card) {
                var newcard = $('<li id="' + card.id + '" class="card card-white' + (hand.playable ? ' card-playable ' : ' ') + card.pack + '">');
                newcard.text(card.text);

                $('#hand').append(newcard);
            });

            if(hand.playable) {
                $('.card-white').on('click', function(e) {
                    var cardID = e.target.id;

                    if(localCards.selected.indexOf(cardID) > -1) {
                        $(e.target).removeClass('card-selected');
                        $(e.target).children('.card-count').remove();

                        localCards.selected = localCards.selected.filter(function(card) {
                            return card !== cardID;
                        });
                    } else if(localCards.selected.length < localCards.black.play) {
                        $(e.target).addClass('card-selected');

                        localCards.selected = localCards.selected.concat(cardID);
                    }

                    if(localCards.black.play > 1) {
                        localCards.selected.forEach(function(card, index) {
                            if($('#' + card).children('.card-count').length < 1) {
                                var count = $('<span class="card-count">');
                                $('#' + card).append(count);
                            }

                            $('#' + card).children('.card-count').text(index + 1);
                        });
                    }
                });
            }
        }
    });

    $('#playCards').on('click', function() {
        socket.emit('playCards', localCards.selected);
    });

    $('#playWinner').on('click', function() {
        socket.emit('czar', localCards.elected);
    });

    socket.on('playedCards', function(players) {
        $('#playedCards').empty();
        localCards.elected = null;

        Object.keys(players).forEach(function(player) {
            var newCardContainer = $('<li class="cards-group" id="' + player + '">');
            var newCardGroup = $('<ul class="nolist">');

            players[player].forEach(function(card) {
                var newCard = $('<li class="card card-white">');
                newCard.text(card.text);

                newCardGroup.append(newCard);
            });

            newCardContainer.append(newCardGroup);
            $('#playedCards').append(newCardContainer);

            $('.cards-group').on('click', function(event) {
                localCards.elected = event.currentTarget.id;

                $('#playedCards').find('li').removeClass('card-selected');
                $('#' + event.currentTarget.id).find('li').addClass('card-selected');
            });
        });
    });

    socket.on('winner', function(winner) {
        $('#playedCards').children('#' + winner).find('li').addClass('winner');
    });

    socket.on('chat', function(msgData) {
        var newline = $('<li>');

        var username = $('<span class="username" style="color: ' + msgData.color + ';">');
        username.text(msgData.username);

        var time = $('<span class="time">');
        var date = new Date();
        time.text(date.getHours() + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()));

        var message = $('<span class="message ' + msgData.type + '">');
        message.html(msgData.msg.replace(/</g, '&lt;').autoLink({target: "_blank"}));

        if(msgData.type === 'user' && lastUsername !== msgData.username) {
            newline.append(username);
        }
        newline.append(message);
        newline.append(time);

        $('#messages > ul').append(newline);
        $('#messages').scrollTop($('#messages > ul').height());

        lastUsername = msgData.username;
    });

    socket.on('Users', function(allUsers) {
        $('#userlist').empty();

        for(var user in allUsers) {
            var newline = $('<li>');
            newline.text(allUsers[user].name + allUsers[user].score);

            $('#userlist').append(newline);
        }
    });

    socket.on('redirect', function(path) {
        window.location = path;
    });

    socket.on('debug', function(data) {
        console.log(data);
    });

    /*
    window.onbeforeunload = function() {
        return 'You will lose all your points, m8!';
    };
*/
});
