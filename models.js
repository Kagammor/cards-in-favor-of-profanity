module.exports = function(Sequelize, sequelize) {
    return {
        Cards: sequelize.define('Card', {
            id: Sequelize.STRING,
            text: Sequelize.TEXT,
            type: Sequelize.ENUM('white', 'black'),
            play: Sequelize.INTEGER,
            pack: Sequelize.STRING
        }),
        Packs: sequelize.define('Pack', {
            id: Sequelize.STRING,
            name: Sequelize.STRING
        })
    };
};
