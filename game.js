var Rooms = {};

var Gamestate = function() {
    return {
        'deck': {
            'white': [],
            'black': []
        },
        'czar': null,
        'black': null,
        'hands': {},
        'played': {},
        'history': []
    };
};
var Room = function(name, settings) {
    return {
        'public': {
            'name': name,
            'status': 0,
            'settings': settings,
            'host': null,
            'users': []
        },
        'private': new Gamestate()
    };
};
var statuses = ['pending', 'playing', 'electing', 'reviewing'];

module.exports = function(es6, config, io, sequelize, models, util, note, uid) {
    sequelize.authenticate()
    .then(function() {
        note('sqlize', '0!', 'Connection authenticated');
    })
    .then(sequelize.sync())
    .then(function() {
        note('sqlize', '0!', 'Models synchronized');
    })
    .catch(function(error) {
        note('sqlize', 2, error);
    });

    io.on('connection', function(socket) {
        var ip = socket.handshake.headers['x-forwarded-for'] || socket.handshake.address.address;

        note('socket', 0, 'Connection established (\'' + socket.id + '\', \'' + ip + '\')');

        socket.on('disconnect', function() {
            note('socket', 0, 'Connection ended (\'' + socket.id + '\', \'' + ip + '\')');
        });
    });

    var Game = {
        'getRoom': function(roomId) {
            if(Rooms[roomId]) {
                return Promise.resolve(Rooms[roomId].public);
            } else {
                return Promise.reject('Room does not exist');
            }
        },
        'getAllRooms': function() {
            var publifiedRooms = Object.keys(Rooms).reduce(function(map, roomId) {
                map[roomId] = Rooms[roomId].public;
                return map;
            }, {});

            return Promise.resolve(publifiedRooms);
        },
        'createRoom': function(name) {
            var newId = uid(),
            nameAvailable = true;

            nameAvailable = Object.keys(Rooms).every(function(roomId) {
                return Rooms[roomId].public.name !== name;
            });

            if(nameAvailable) {
                Rooms[newId] = new Room(name, config.settings.defaults);

                return Promise.resolve(newId);
            } else {
                return Promise.reject('Roomname already in use');
            }
        },
        'destroy': function(roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            delete Rooms[roomId];

            return Promise.resolve(roomId);
        },
        'join': function(user, roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            user.score = 0;

            Rooms[roomId].public.users = Rooms[roomId].public.users.concat(user);

            if(Rooms[roomId].public.users.length <= 1 && Rooms[roomId].public.users[0] === user) {
                Rooms[roomId].public.host = user.id;

                return Promise.resolve(true);
            } else {
                return Promise.resolve(false);
            }
        },
        'leave': function(userId, roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            Rooms[roomId].public.users = Rooms[roomId].public.users.filter(function(userOfAll) {
                return userOfAll.id !== userId;
            });

            delete Rooms[roomId].private.hands[userId];

            var usersWithHand = Object.keys(Rooms[roomId].private.hands).filter(function(hand) {
                return Rooms[roomId].private.played[hand] ? false : true;
            });

            if(Rooms[roomId].public.users.length > 0) {
                return Game.electHost(roomId, userId)
                .then(function() {
                    if(usersWithHand.length > 0) {
                        return Game.electCzar(roomId, userId)
                        .then(function(czar) {
                            return Game.getHand(czar, roomId);
                        })
                        .then(function(hand) {
                            return Promise.resolve({
                                'host': Rooms[roomId].public.host,
                                'czar': hand
                            });
                        });
                    }

                    if(Rooms[roomId].public.status === 1 && Rooms[roomId].public.users.length > 0) {
                        return Game.newRound(roomId)
                        .then(function(cards) {
                            return Promise.resolve({
                                'host': Rooms[roomId].public.host,
                                'cards': cards
                            });
                        })
                        .catch(function(error) {
                            return Promise.resolve({
                                'host': Rooms[roomId].public.host
                            });
                        });
                    }

                    return Promise.resolve({
                        'host': Rooms[roomId].public.host
                    });
                });
            }

            return Game.destroy(roomId);
        },
        'getUser': function(userDetail, roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            var user = Rooms[roomId].public.users.find(function(user) {
                return user.id === userDetail || user.name === userDetail;
            });

            return user ? Promise.resolve(user) : Promise.reject('User not found');
        },
        'getAllUsers': function(roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            return Promise.resolve(Rooms[roomId].public.users);
        },
        'isInRoom': function(userId, roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            var isUser = function(userIdInRoom) {
                return userIdInRoom.id === userId;
            };

            return Promise.resolve(Rooms[roomId].public.users.find(isUser) ? true : false);
        },
        'isHost': function(userId, roomId) {
            if(!Rooms[roomId]) {
                return false;
            }

            return Rooms[roomId].public.host === userId;
        },
        'isCzar': function(userId, roomId) {
            if(!Rooms[roomId]) {
                return false;
            }

            return Rooms[roomId].private.czar === userId;
        },
        'electHost': function(roomId, userId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            if(typeof userId === 'undefined' || Game.isHost(userId, roomId)) {
                Rooms[roomId].public.host = Rooms[roomId].public.users[Math.floor(Math.random() * Rooms[roomId].public.users.length)].id;
            }

            return Promise.resolve(Rooms[roomId].public.host);
        },
        'electCzar': function(roomId, userId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            var currentCzar = Rooms[roomId].public.users.findIndex(function(user) {
                return user.id === Rooms[roomId].private.czar;
            });

            if(typeof userId === 'undefined' || Game.isCzar(userId, roomId)) {
                if(currentCzar > -1 && Rooms[roomId].public.users[currentCzar + 1]) {
                    Rooms[roomId].private.czar = Rooms[roomId].public.users[currentCzar + 1].id;
                } else {
                    Rooms[roomId].private.czar = Rooms[roomId].public.users[0].id;
                }
            }

            return Promise.resolve(Rooms[roomId].private.czar);
        },
        'getSettings': function(roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            return Promise.resolve(Rooms[roomId].public.settings);
        },
        'setSettings': function(newSettings, roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            Rooms[roomId].public.settings = newSettings;

            return Promise.resolve(Rooms[roomId].public.settings);
        },
        'getAllPacks': function() {
            return new Promise(function(resolve, reject) {
                var packs = [];

                models.Packs
                    .findAll()
                    .then(function(allPacks) {
                        allPacks.forEach(function(pack) {
                            packs = packs.concat(pack.dataValues);
                        });

                        resolve(packs);
                    }, function(error) {
                        reject(error);
                    });
            });
        },
        'composeDeck': function(roomId) {
            return new Promise(function(resolve, reject) {
                if(Rooms[roomId]) {
                    models.Cards
                    .findAll({'where': {'pack': Rooms[roomId].public.settings.packs}})
                    .then(function(allCards) {
                        Rooms[roomId].private.deck.white = [];
                        Rooms[roomId].private.deck.black = [];

                        allCards.forEach(function(card) {
                            if(card.dataValues.type === 'white') {
                                Rooms[roomId].private.deck.white = Rooms[roomId].private.deck.white.concat(card.dataValues);
                            }
                            if(card.dataValues.type === 'black') {
                                Rooms[roomId].private.deck.black = Rooms[roomId].private.deck.black.concat(card.dataValues);
                            }
                        });

                        resolve(Rooms[roomId].private.deck);
                    })
                    .catch(function(error) {
                        reject(error);
                    });
                } else {
                    reject('Room does not exist');
                }
            });
        },
        'shuffleDeck': function(roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            var shuffle = function(cards) {
                // Fisher-Yates shuffle
                var m = cards.length, t, i;

                while(m) {
                    i = Math.floor(Math.random() * m--);

                    t = cards[m];
                    cards[m] = cards[i];
                    cards[i] = t;
                }

                return cards;
            };

            Rooms[roomId].private.deck.white = shuffle(Rooms[roomId].private.deck.white);
            Rooms[roomId].private.deck.black = shuffle(Rooms[roomId].private.deck.black);

            return Promise.resolve(Rooms[roomId].private.deck);
        },
        'drawBlackCard': function(roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            Rooms[roomId].private.black = Rooms[roomId].private.deck.black.splice(0, 1)[0];

            return Promise.resolve(Rooms[roomId].private.black);
        },
        'drawHands': function(roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            Rooms[roomId].public.users.forEach(function(user) {
                Rooms[roomId].private.hands[user.id] = Rooms[roomId].private.hands[user.id] || [];

                for(var i=Rooms[roomId].private.hands[user.id].length;i<10;i+=1) {
                    Rooms[roomId].private.hands[user.id] = Rooms[roomId].private.hands[user.id].concat(Rooms[roomId].private.deck.white.splice(0, 1));
                }
            });

            return Promise.resolve(Rooms[roomId].private.hands);
        },
        'getHand': function(userId, roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            if(Rooms[roomId].private.hands[userId]) {
                return Promise.resolve({'id': userId, 'cards': Rooms[roomId].private.hands[userId]});
            } else {
                return Promise.reject('User not found');
            }
        },
        'getAllHands': function(roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            return Promise.resolve(Rooms[roomId].private.hands);
        },
        'playCards': function(cardIds, userId, roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }
            if(!Rooms[roomId].private.hands[userId]) {
                return Promise.reject('User has no cards');
            }

            var roundId = Rooms[roomId].private.round;

            if(!round) {
                return Promise.reject('No ongoing round');
            }
            if(Rooms[roomId].private.played[roundId][userId]) {
                return Promise.reject('Cards already played');
            }
            if(cardIds.length < Rooms[roomId].private.black.play) {
                return Promise.reject('Not enough cards played');
            }

            var matchedCards = cardIds.map(function(card) {
                var inHand = function(cardInHand) {
                    return card === cardInHand.id;
                };

                return Rooms[roomId].private.hands[userId].find(inHand);
            });

            if(!matchedCards.every(Boolean)) {
                return Promise.reject('Card not in hand');
            }

            // Remove played cards from hand
            Rooms[roomId].private.hands[userId] = Rooms[roomId].private.hands[userId].filter(function(card) {
                return cardIds.indexOf(card.id) === -1;
            });

            Rooms[roomId].private.played[roundId][userId] = matchedCards;

            if(Object.keys(Rooms[roomId].private.played[roundId]).length === Object.keys(Rooms[roomId].private.hands).length - 1) {
                return Promise.resolve({'played': Rooms[roomId].private.played[roundId], 'hand': Rooms[roomId].private.hands[userId]});
            }

            // Empty cards to keep them hidden
            var fathomCards = {};

            Object.keys(Rooms[roomId].private.played[roundId]).forEach(function(userId) {
                fathomCards[userId] = Rooms[roomId].private.played[roundId][userId].map(function(card) {
                    return {};
                });
            });

            return Promise.resolve({'played': fathomCards, 'hand': Rooms[roomId].private.hands[userId]});
        },
        'electWinner': function(userId, roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            var round = Rooms[roomId].private.round;

            var winnerUndecided = Rooms[roomId].private.history.every(function(set) {
                return set.round !== round;
            });

            if(!winnerUndecided) {
                return Promise.reject('Winner already elected');
            }

            var userIndex = Rooms[roomId].public.users.findIndex(function(user) {
                return user.id === userId;
            });

            if(userIndex > -1) {
                Rooms[roomId].public.users[userIndex].score += 1;

                Rooms[roomId].private.history = Rooms[roomId].private.history.concat({
                    'round': round,
                    'user': Rooms[roomId].public.users[userIndex].name,
                    'cards': Rooms[roomId].private.played[round][userId]
                });

                Rooms[roomId].public.status = 3;

                return Promise.resolve(userId);
            } else {
                return Promise.reject('User not found');
            }
        },
        'getStatus': function(roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            return Promise.resolve(statuses[Rooms[roomId].public.status]);
        },
        'setStatus': function(status, roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            if(typeof status === 'number' && status < statuses.length) {
                Rooms[roomId].public.status = status;

                return Promise.resolve(Rooms[roomId].public.status);
            }

            if(statuses.indexOf(status) > -1) {
                Rooms[roomId].public.status = status.indexOf(status);

                return Promise.resolve(Rooms[roomId].public.status);
            }

            return Promise.reject('Invalid status');
        },
        'reset': function(roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            Rooms[roomId].private = new Gamestate();

            Rooms[roomId].public.users.forEach(function(user, index) {
                Rooms[roomId].public.users[index].score = 0;
            });

            return Promise.resolve(Rooms[roomId].private);
        },
        'start': function(userId, roomId) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            return new Promise(function(resolve, reject) {
                return Game.isHost(userId, roomId) ? resolve() : reject('User is not host');
            })
            .then(Game.reset.bind(null, roomId))
            .then(Game.composeDeck.bind(null, roomId))
            .then(Game.shuffleDeck.bind(null, roomId))
            .then(Game.newRound.bind(null, roomId));
        },
        'newRound': function(roomId, delay) {
            if(!Rooms[roomId]) {
                return Promise.reject('Room does not exist');
            }

            return new Promise(function(resolve, reject) {
                setTimeout(function() {
                    round = new Date().getTime();

                    Rooms[roomId].private.round = round;
                    Rooms[roomId].private.played[round] = {};

                    Rooms[roomId].public.status = 1;

                    if(Rooms[roomId].public.users.length > 1) {
                        Game.electCzar(roomId)
                        .then(function() {
                            return Promise.all([
                                Game.drawBlackCard(roomId),
                                Game.drawHands(roomId)
                            ]);
                        })
                        .then(function(cards) {
                            resolve(cards);
                        })
                        .catch(function(error) {
                            reject(error);
                        });
                    } else {
                        reject('Not enough players');
                    }
                }, typeof delay === 'number' ? delay : 0);
            });
        }
    };

    return Game;
};
