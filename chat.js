var generateUsername = require('./modules/namegen.js'),
    generateColor = function(range) {
        range = range || [0, 255];
        return parseInt(Math.random() * (range[1] - range[0] + 1) + range[0], 10).toString(16);
    };

module.exports = function(es6, config, io, Game, util, note) {
    io.on('connection', function(socket) {
        socket.user = socket.user || {'id': socket.id};

        socket.user.name = generateUsername(true);
        socket.user.color = '#' + generateColor([100, 200]) + generateColor([100, 200]) + generateColor([100, 200]);

        socket.on('joinRoom', function(room) {
            socket.join(room);

            io.to(room).emit('chat', {
                'type': 'broadcast',
                'msg': socket.user.name + ' joined!'
            });

            Game.getAllUsers(room)
            .then(function(users) {
                io.to(room).emit('Users', users);
            })
            .catch(function(error) {
                note('cifop', 2, error);
            });
        });

        var throttleStack = [];

        socket.on('chat', function(msgData) {
            throttleStack = throttleStack.concat(new Date().getTime());

            if(throttleStack.length > config.chat.throttle.span) {
                throttleStack = throttleStack.slice(1);
            }

            // Measure time difference between messages for throttling once throttle stack is filled
            if(throttleStack[config.chat.throttle.span - 1] - throttleStack[0] > config.chat.throttle.time || throttleStack.length < config.chat.throttle.span) {
                io.to(socket.user.room).emit('chat', {
                    'type': 'user',
                    'msg': msgData.msg.substring(0, 140),
                    'username': socket.user.name,
                    'color': socket.user.color
                });
            } else {
                io.to(socket.id).emit('chat', {
                    'type': 'personal',
                    'msg': 'Please try to be less verbose!'
                });
            }
        });

        socket.on('disconnect', function() {
            if(socket.user.room) {
                Game.getAllUsers(socket.user.room)
                .then(function(users) {
                    io.to(socket.user.room).emit('chat', {
                        'type': 'broadcast',
                        'msg': socket.user.name + ' left!'
                    });

                    io.to(socket.user.room).emit('Users', users);
                })
                .catch(function(error) {
                    note('cifop', 2, error);
                });
            }
        });
    });
};
