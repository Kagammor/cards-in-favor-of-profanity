module.exports = function(grunt) {
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            all: {
                options: {
                    force: true
                },
                files: {
                    src: [
                        '*.js',
                        'assets/js/*.js',
                        '!assets/js/jquery.js',
                        '!assets/js/socket.io.js',
                        '!assets/js/require.js',
                        '!assets/js/autolink.js'
                    ]
                }
            }
        },
        scsslint: {
            options: {
                excludeLinter: 'Indentation'
            },
            allFiles: [
                'assets/css/**/*.scss',
            ]
        },
        jsonlint: {
            sample: {
                src: ['*.json']
            }
        },
        sass: {
            options: {
                outputStyle: 'collapsed'
            },
            dist: {
                files: {
                    'assets/css/main.css': 'assets/css/main.scss'
                }
            }
        },
        watch: {
            jshint_watch: {
                files: ['*.js', 'assets/js/*.js'],
                tasks: ['jshint:all']
            },
            scsslint_watch: {
                files: ['assets/css/**/*.scss'],
                tasks: ['scsslint']
            },
            jsonlint_watch: {
                files: ['*.json'],
                tasks: ['jsonlint']
            },
            sass_watch: {
                files: ['assets/css/**/*.scss'],
                tasks: ['sass']
            }
        }
    });

    grunt.registerTask('default', ['watch']);
};
